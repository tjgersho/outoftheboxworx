<!DOCTYPE html>
<html lang="en">
<head>
    <base href="/">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1, minimum-scale=1"/> -->
    <link rel="shortcut icon" href="/images/favicons/favicon.png" type="image/x-icon" />

    <title>Out of the Box Worx</title>

    <!-- Fonts -->
    <link rel="stylesheet" href="css/all.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700">

  
</head>
<body id="app-layout"  ng-app="paradigmMotion" class="ng-cloak" ng-controller="MainController">
<div id="bgpicture_filter" style="position:fixed; width:100%; height:100%; opacity: 0.5; background-color:#ffffff; z-index:-9999"></div>

	<nav-menu></nav-menu>

	 <div class="container">
      		  <div class="row">

 			 <ui-view> 
                                   <div class="text-center">
       				 <img src='images/uicontentLoading.gif' style="position: absolute; top: 50%;left: 50%; width: 100px;height: 100px; margin-top: -50px;  margin-left: -50px;" >
      				  </div>

       			   </ui-view>

   		 </div>
 	</div>
 



<foo-ter></foo-ter>


     <script src="js/all.js?v=<?php echo filemtime('js/all.js'); ?>"></script>
   
    </body>
</html>
