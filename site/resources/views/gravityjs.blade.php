<!DOCTYPE html>
<html lang="en">
<head>
    <base href="/">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1, minimum-scale=1"/> -->
    <link rel="shortcut icon" href="/images/favicons/favicon.png" type="image/x-icon" />

    <title>Paradigm Motion, LLC</title>

    <!-- Fonts -->
    <link rel="stylesheet" href="css/all.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700">

  
</head>
<body id="app-layout"  ng-app="paradigmMotion" class="ng-cloak" ng-controller="MainController">

	<div id="gravContainer" style="width:100%; padding:0; margin:0;"></div>

     <script src="js/gravityjs.js"></script>
   
    </body>
</html>
