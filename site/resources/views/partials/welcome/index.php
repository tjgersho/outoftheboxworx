<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">CODE WORX</div>

                <div class="panel-body">
                    Out of the Box Worx is something of a portfolio of projects developed by or contributed by Paradigm Motion and Travis Gershon.

		    <br /><br />
		
			<div class="list-group">
                           <div class="list-group-item disabled">Recent projects include:</div>
                     

			    <a class="list-group-item"  href="http://cirrusidea.com"> CirrusIdea.com </a>
			    <a class="list-group-item"  href="https://paradigmmotion.com"> ParadigmMotion.com </a>
                         <a class="list-group-item" href="https://peakbev.com"> Peakbev.com </a>
                         <a class="list-group-item" href="http://kyndhub.com"> KyndHub.com </a>
                         <a class="list-group-item" href="https://outoftheboxworx.com/gravifun"> Gravifun </a> 
                          
                     </div>
                   <br /><br />
<div class="container">
                  Check out the latest in repositories:<br />
			<a href="https://github.com/tjgersho">GitHub</a><br /><br />
                     <a href="https://bitbucket.org/tjgersho/">BitBucket</a><br /><br />

</div> 
                </div>
            </div>
        </div>
    </div>
</div>
 