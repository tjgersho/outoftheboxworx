!function(jQuery){
    jQuery(window).resize(function() {
        if (jQuery(window).width() > 768) {
            jQuery('nav .nav').removeAttr('style');
        }
    });
    if (jQuery(window).width() <= 768) {
        jQuery('nav .nav li.menu-item-has-children').append("<span></span>");
        var pullchild = jQuery('nav .nav li.menu-item-has-children > span');
        jQuery(pullchild).on('click', function() {
            jQuery(this).toggleClass('show');
            jQuery(this).siblings('ul.sub-menu').toggleClass('show');
        });
    } else {
        jQuery('nav .nav li').hover(function() {
            jQuery(this).children('ul').slideDown('fast');
        }, function() {
            jQuery(this).children('ul').slideUp('fast');
        }); /* Hover menu Effect*/
        jQuery('nav .nav ul li').hover(function() {
            jQuery(this).find('ul:first').css({visibility: 'visible',display: 'none'}).show(400);
        }, function() {
            jQuery(this).find('ul:first').css({visibility: 'hidden'});
        });
    };
    /*Contact form 7*/
    jQuery(document).ready(function(){
        jQuery('.wpcf7-not-valid-tip').on({"mouseenter": function () {
            jQuery(this).animate({opacity:0},300,function(){
                jQuery(this).remove();
            });
        },"mouseleave": function(){
            //mouseout
        }
        });
    });
    /*------header-------*/
    var w=$(window).width();
    if(w>=992){
        jQuery(window).scroll(function () {
            if (jQuery(window).scrollTop() > 140) {
                jQuery("header").addClass("scroll");
            }else{
                jQuery("header").removeClass("scroll");
            }
        });
    };

jQuery(document).on('click','.navbar-collapse.in',function(e) {
    if( $(e.target).is('a')) {
        $(this).collapse('hide');
    }
});


}(jQuery);
