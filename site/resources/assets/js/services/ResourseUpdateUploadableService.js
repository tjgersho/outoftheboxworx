angular.module('services').factory('ResourseUpdateUploadable', [ '$rootScope',
	function serviceInterface($rootScope) {

		function generateUpdateMethod() {
			var debug = false;

			return {
					method: 'POST',
					isArray: false,
					//also handle file upload
					transformRequest: function(data, headersGetter) {
						debug && console.log('send payload: ');
						debug && console.log(data);

						//Inspired by http://stackoverflow.com/questions/21115771/angularjs-upload-files-using-resource-solution
						if (data === undefined) {
							return data;
						}

						var fd = new FormData();
						angular.forEach(data, function(value, key) {
							if (value instanceof FileList) {
								if (value.length == 1) {
									fd.append(key, value[0]);
								} else {
									angular.forEach(value, function(file, index) {
										fd.append(key + '_' + index, file);
									});
								}
							} else {
								//jsonify objects to transfer to server
								if (angular.isObject(value) && !(value instanceof File)) {
									fd.append(key, angular.toJson(value));
								} else {
									fd.append(key, value);
								}
							}
						});

						return fd;
					},
					headers: {
						'Content-Type': undefined
					},

					transformResponse : function(data, headersGetter) {
						data = angular.fromJson(data);

						$rootScope.$broadcast('companySaved', data);

						return data;
					}

					// interceptor: {
					// 	'response': function(response) {
					// 					$rootScope.$broadcast('companySaved', response);
					// 					return response;
					// 				}
					// 			}
				};
		}

		//interface
		return {
			generateUpdateMethod: generateUpdateMethod
		}
	}
]);