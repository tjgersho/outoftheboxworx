;(function() {
	'use strict';
	angular.module('services').factory('AuthService', ['$rootScope', '$localStorage', '$state', '$modal', 'User', AuthService]);

	function AuthService($rootScope, $localStorage, $state, $modal, User) {
		var service = {};

		service.$login = function (username, password, onSuccess) {
			var user = new User({
				username: username,
				password: password
			});

			return user.$login(function(user) {
                        console.log("USER AUTH RESPONSE");
		          console.log(user);
				$localStorage.token = user.token;
				service.$getAuthenticatedUser();
			}, function(err) {
				if (err.status == 401) {
					alert('Wrong username or password!');
				}
				console.log(err);
			});
		}

		service.$register = function (email, name, password, onSuccess) {
			var user = new User({
				email: email,
				name: name,
				password: password
			});

			return user.$register(function(user) {
				var opportunityView = $modal.open({
					templateUrl : "/partials/auth/register-success",
					animation : false,
					//scope : $scope,
					windowClass: "register-success-modal"
				});

				//without this the modal does not show after second try or not at all
				$scope.$apply();

				//$state.go('login');
			}, function(err) {
				if (err.status == 401) {
					alert('Email already exists!');
				}
				console.log(err);
			});
		}

		/**
		 * Query the authenticated user by the Authorization token from the header.
		 *
		 * @param user {object} If provided, it won't query from database, but take this one.
		 * @returns {null}
		 */
		service.$getAuthenticatedUser = function(user) {

			if (user) {
				console.log('s');
				service.authenticatedUser = user;
				return;
			}

			if (typeof $localStorage.token === 'undefined') {
				return null;
			}

			return new User().$getByToken(function(user) {
                              //console.log("GET BY TOKEN");
				  //console.log(user);
				service.authenticatedUser = user;
				$rootScope.$emit('userAuthenticated', user);
			}, function(err) {
				console.log(err);
			});
		};

		service.logout = function() {
			delete $localStorage.token;
			service.authenticatedUser = null;
		};

		service.canManageCompanies = function () {
			return service.checkRole('site-admin');
		}

		service.canManageGroups = function () {
			return service.checkRole('site-admin');
		}

		service.canManageUsers = function (company) {
			//console.log("canManageUsers of " + company);
			return service.checkRole('site-admin') || service.canEditCompany(company);
		}

		service.checkRole = function(roleToCheck){
			//console.log(findInArrayForKey(service.authenticatedUser.roles, 'name', roleToCheck));
			return service.authenticatedUser && !!findInArrayForKey(service.authenticatedUser.roles, 'name', roleToCheck);
		}

		service.canEditCompany = function (company) {
			if (!company) {
				return false;
			}

			var roleName = 'super-admin-of-company-' + company.id;
			//console.log(roleName);
			return service.authenticatedUser && !!findInArrayForKey(service.authenticatedUser.roles, 'name', roleName);
		}


		function findInArrayForKey(array, key, value) {
			for (var i = array.length - 1; i >= 0; i--) {
				if( array[i][key] == value ){
					return array[i];
				}
			};

			return false;
		}

		service.$getAuthenticatedUser();

		return service;
	}

})();