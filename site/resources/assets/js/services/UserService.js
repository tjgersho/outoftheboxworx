angular.module('services').factory('User', ['$resource', 'ResourseUpdateUploadable',
  function($resource, ResourseUpdateUploadable) {
    return $resource('/api/v1/user/:userId', {
      userId: '@id'
    }, {
      update: ResourseUpdateUploadable.generateUpdateMethod(),
      invite: {
        method: 'POST',
        url: '/api/v1/user/invite'
      },
      login: {
        method: 'POST',
        url: '/api/v1/user/login'
      },
      register: {
        method: 'POST',
        url: '/api/v1/user/register'
      },
      getByToken: {
        method: 'GET',
        url: '/api/v1/user/getByToken'
      },
      all: {
        method: 'GET',
        url: '/api/v1/user/',
        isArray: false
      }
    });
  }
]);