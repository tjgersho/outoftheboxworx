;(function() {
	'use strict';

	angular.module('controllers').controller('SplashController', 
                    ['$window', '$rootScope', '$scope', '$filter', '$state', '$stateParams', 
                      '$anchorScroll', '$modal', '$http', '$localStorage', '$location', '$interval',
                         'AuthService', 'User', 'ngTableParams', SplashController]);

function SplashController($window, $rootScope, $scope, $filter, $state, $stateParams, $anchorScroll, $modal, $http, $localStorage, $location, $interval, AuthService, User,  ngTableParams) {
var gravElement;   
$scope.inithomescreen = function(){
var screen_width =  $window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;

//var screen_height = $window.innerHeight|| document.documentElement.clientHeight || document.body.clientHeight;
var screen_height = $window.innerHeight|| document.documentElement.clientHeight || document.body.clientHeight;
//var screen_height =  $window.screen.availHeight
if($window.screen.availHeight<screen_height){
	screen_height = $window.screen.availHeight;
}

if($window.screen.height<screen_height){
	screen_height = $window.screen.height;
}

if((screen_width>1.5*screen_height && screen_height<500) || screen_height<350){
	
	var sizing = screen_height;

var adjustsize = 100*Math.pow(0.2*sizing,0.2);

$('#homegraphic').css('width', adjustsize+'px');

var logo_height =  $('#homegraphic').height();
 $('#homegraphic').css({'top':screen_height*0.2+'px', 'font-size':0.18*screen_width +'px'});
var headerwidth =  $('#homegraphic').width();
 $('#homegraphic').css('left', screen_width/2-headerwidth/2 +'px' );


$('#gravifunIcon').css('width', 0.2*screen_width + 'px');
var gravifunIconwidth = $('#gravifunIcon').width();

$('#gravifunIcon').css('height', gravifunIconwidth + 'px');
var gravifunIconheight = $('#gravifunIcon').height();
$('#gravifunButton').css({'top':screen_height*0.93- gravifunIconheight + 'px', 'left': gravifunIconwidth*0.75+'px'});


$('#gravifunGooglePlay').css('width', 0.2*screen_width + 'px');
var googleIconWidth = $('#gravifunGooglePlay').width();

//$('#gravifunGooglePlay').css('height', googleIconWidth + 'px');
var  googleIconHeight = $('#gravifunGooglePlay').height();
$('#gravifunGooglePlayLink').css({'top':screen_height*0.98- googleIconHeight + 'px', 'left': googleIconWidth*0.75+'px'});



$('#gravifunApple').css('width', 0.2*screen_width + 'px');
var appleIconWidth = $('#gravifunApple').width();

$('#gravifunApple').css('height', 40/160*appleIconWidth + 'px');
var  appleIconHeight = $('#gravifunApple').height();
$('#gravifunAppleLink').css({'top':screen_height*0.93 - 1.2*appleIconHeight -gravifunIconwidth+ 'px', 'left': appleIconWidth*0.75+'px'});


}else{

var sizing = screen_width;


var adjustsize = 100*Math.pow(0.2*sizing,0.2);

$('#homegraphic').css('width', adjustsize+'px');

var logo_height =  $('#homegraphic').height();
 $('#homegraphic').css({'top':screen_height*0.2+'px', 'font-size':0.18*screen_width +'px'});
var headerwidth =  $('#homegraphic').width();
 $('#homegraphic').css('left', screen_width/2-headerwidth/2 +'px' );


$('#gravifunIcon').css('width', 0.2*screen_width + 'px');
var gravifunIconwidth = $('#gravifunIcon').width();

$('#gravifunIcon').css('height', gravifunIconwidth + 'px');
var gravifunIconheight = $('#gravifunIcon').height();
$('#gravifunButton').css({'top':screen_height*0.93- gravifunIconheight + 'px', 'left': gravifunIconwidth*0.75+'px'});


$('#gravifunGooglePlay').css('width', 0.2*screen_width + 'px');
var googleIconWidth = $('#gravifunGooglePlay').width();

//$('#gravifunGooglePlay').css('height', googleIconWidth + 'px');
var  googleIconHeight = $('#gravifunGooglePlay').height();
$('#gravifunGooglePlayLink').css({'top':screen_height*0.98- googleIconHeight + 'px', 'left': googleIconWidth*0.75+'px'});



$('#gravifunApple').css('width', 0.2*screen_width + 'px');
var appleIconWidth = $('#gravifunApple').width();

$('#gravifunApple').css('height', 40/160*appleIconWidth + 'px');
var  appleIconHeight = $('#gravifunApple').height();
$('#gravifunAppleLink').css({'top':screen_height*0.93 - 1.2*appleIconHeight -gravifunIconwidth+ 'px', 'left': appleIconWidth*0.75+'px'});



}


};



//set random bacground image
var bgimgs = [
'images/bgimage/abstract-design.jpg', 
'images/bgimage/158.jpg', 
'images/bgimage/bg1.png', 
'images/bgimage/code.jpg', 
'images/bgimage/ebg1.jpg',
'images/bgimage/mechEng.jpg'];

var rand = Math.floor((Math.random() * 4)); 
$('body').css('background-image', 'url("/'+bgimgs[rand]+'")');


$(window).resize(function(){
$scope.inithomescreen();
});

$(window).on( "orientationchange", function() { 
$scope.inithomescreen();
});




var bgcount = 0;

var bgOntimer =  $interval(function(){
	var rand = Math.floor((Math.random() * 4)); 
//console.log(bgimgs[rand]);

                $('#bgpicture_filter').animate({opacity:1.0}, 600, function(){ 
                    
                    $('<img/>').attr('src', bgimgs[rand]).load(function() {
   			$(this).remove(); // prevent memory leaks as @benweet suggested
  			$('body').css('background-image', 'url("/'+bgimgs[rand]+'")');
  			$('#bgpicture_filter').animate({opacity:0.3}, 600);
			
			bgcount++;
			if (bgcount>4) {
				console.log('bg fade done');
                      $interval.cancel(bgOntimer);
			 $('#bgpicture_filter').animate({opacity:0.3}, 600);
			  $('body').css('background-image', 'url("/images/bgimage/mainbg1.png")');
                      }
  		  });
								
		});
       
  
}, 5000);

 
$scope.$on("$destroy", function() {
        if (bgOntimer) {
            $interval.cancel(bgOntimer);
        }
});




$scope.learnmoreclick = function(){
$state.go('/welcome');
};
 

$scope.gotomainclick = function(){
$location.path('/landing/1');
};





$scope.inithomescreen();



}



})();