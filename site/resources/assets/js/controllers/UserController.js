;(function() {
	'use strict';
	angular.module('controllers').controller('UserController', ['$scope', '$state', 'AuthService', UserController]);

	function UserController($scope, $state, AuthService) {
		$scope.auth = AuthService;

		$scope.login = function() {
			$scope.auth.$login(this.username, this.password).then(goHome);
		};

		$scope.register = function() {
			$scope.auth.$register(this.email, this.name, this.password, this.confirm_password);//.then(goHome);
		};

		function goHome() {
			$state.go('home');
		}
	}
})();