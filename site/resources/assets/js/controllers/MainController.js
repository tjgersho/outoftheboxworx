;(function() {
	'use strict';

	angular.module('controllers').controller('MainController', ['$rootScope', '$scope', '$filter', '$state', '$stateParams', '$anchorScroll', '$modal', '$http', '$localStorage', '$location',
											 'AuthService', 'User', 'ngTableParams', MainController]);

	function MainController($rootScope, $scope, $filter, $state, $stateParams, $anchorScroll, $modal, $http, $localStorage, $location, AuthService, User,  ngTableParams) {
		$scope.auth = AuthService;
		$scope.$state = $state;
		$scope.selections = {};
              $scope.showNav = false;
        
	       $rootScope.$on('$stateChangeStart', handleStateChanges);

		$rootScope.$on('userAuthenticated', onUserAuthenticated);
             

		function handleStateChanges(event, toState, toParams, fromState, fromParams) {
			//we are in the same state with the same params so nothing to be done
			//TODO: is this required?
                    //handelLoginRedirect(this, arguments);

			if (angular.equals(toState, fromState) && angular.equals(toParams, fromParams)) {
				return true;
			}
                   
                     
                    

                    if(toState.name === 'loginredirect'){
                       $localStorage.token = toParams.token;
                       $scope.auth.$getAuthenticatedUser().then(function(resp){ 
                            console.log("Get Auth User Resp");
				console.log(resp);
                            console.log("USER");
		               console.log($scope.user);
                            $scope.$state.go('user', {userId: $scope.user.id});
                        });
                      
           
                     } 



			//default content width
			$scope.contentViewColumns = 9;

			console.log(toState.name);
                        
                     handleSplashState.apply(this, arguments) ||


			handleLogoutState.apply(this, arguments);
                     

		}


		function  handleSplashState(event, toState, toParams, fromState, fromParams) {
			if (toState.name != 'splash') {
                            $scope.showNav = true;
                       
			            $('body').css('background-image', 'url("/images/bgimage/mainbg1.png")');
                            

				return false
			}
			console.log("SHOW NAV");
                     $scope.showNav = false;

			return true;
		}


		function handleLogoutState(event, toState, toParams, fromState, fromParams) {
			if (toState.name != 'logout') {
				return false
			}
			//clean up
			$scope.selections.company = null;
			$scope.status_updates = null;
			$scope.groups = null;
			$scope.status_updates = null;
			AuthService.logout();

			console.log('logoutx');

			event.preventDefault();

			$state.go('login');

			return true;
		}

		function onUserAuthenticated(user) {
			$scope.user = $scope.auth.authenticatedUser;
			$scope.selections.company = $scope.user.companies[0];
			$scope.groups = $scope.selections.company.groups;
                }

		//get object from array input with given id
		function getById(input, id) {
			//source: http://stackoverflow.com/questions/15610501/in-angular-i-need-to-search-objects-in-an-array
		    var i=0, len = input.length;
		    for(; i < len; ++i ) {
		      if (+input[i].id == +id) {
		        return input[i];
		      }
		    }
		    return null;
		}
	}
}());