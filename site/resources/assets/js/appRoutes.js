angular.module('appRoutes', []).config(['$stateProvider', '$urlRouterProvider', '$locationProvider', '$httpProvider', '$injector',
	function($stateProvider, $urlRouterProvider, $locationProvider, $httpProvider, $injector) {

		//$urlRouterProvider.otherwise('/home');

		$urlRouterProvider.otherwise(function($injector, $location)
		{
			window.location = $location.absUrl();
		});

		$stateProvider
       
                     .state('splash', {
				url: '/',
				templateUrl: '/partials/splash/',
 				controller: 'SplashController'
			})
                     .state('welcome', {
				url: '/welcome',
				templateUrl: '/partials/welcome/',
			})
                     .state('hardware', {
				url: '/hardware',
				templateUrl: '/partials/hardware/',
			})
                     .state('software', {
				url: '/software',
				templateUrl: '/partials/software/',
			})
			.state('login', {
				url: '/login',
				templateUrl: '/partials/auth/login',
				controller: 'UserController'
			})
			.state('signup', {
				url: '/signup',
				templateUrl: '/partials/auth/register',
				controller: 'UserController'
			})
			.state('logout', {
				url: '/logout',
				controller:'UserController'
			})
			.state('home', {
				url: '/home',
				templateUrl: '/partials/home'
			})
                     
			.state('user', {
				url: '/user/:userId',
				templateUrl: '/partials/status_updates'
			})
			.state('admin', {
				url: '/admin',
				templateUrl: '/partials/admin'
			});

		$locationProvider.html5Mode(true);

		$httpProvider.interceptors.push(['$rootScope', '$q', '$localStorage',
			function($rootScope, $q, $localStorage) {
				return {
					request: function(config) {
						config.headers = config.headers || {};
						if ($localStorage.token) {
							config.headers.Authorization = 'Bearer ' + $localStorage.token;
						}
						return config;
					},
					response: function(res) {
						if (Math.floor(res.status / 100)  == 4) {
							// Handle unauthenticated user.
							$stateProvider.go('login');
						}
						return res || $q.when(res);
					}
				};
			}
		]);
	}
]);