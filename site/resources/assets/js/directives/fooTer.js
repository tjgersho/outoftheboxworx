;(function() {
	'use strict';

angular.module('directives').directive('fooTer', function() {

	var controller = ['$scope',  function($scope) {
                    //alert("Footer Controller");
			}];

	return {
		restrict: 'E',
		controller: controller,
		templateUrl: '/partials/footer/'
	};
});

})();