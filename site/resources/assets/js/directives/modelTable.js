angular.module('directives').directive('modelTable', function() {

	var controller = ['$scope', '$injector', 'ngTableParams', function($scope, $injector, ngTableParams) {

		var modelService = $injector.get($scope.modelService);

		$scope.$on('companySaved', function(event, data) {
			console.log('company saved event received');
			console.log(data);

			reloadTable();
		});


		//Build the table (using ngtable)
		$scope.tableParams = new ngTableParams({
			page: 1, // show first page
			count: 10 // count per page
		}, {
			getData: function($defer, params) {
				modelService.all({
					sort: params.sorting(),
					filter: params.filter(),
					page: params.page(),
					per_page: params.count()
				}, function(data, headersGetter) {
					params.total(data.total);
					$defer.resolve(data.data);
					//required to update the total of elements in template
					if (!$scope.$$phase && !$scope.$root.$$phase) {
						$scope.$apply();
					}

					$scope.total = $scope.tableParams.total();
				});
			}
		});

		//Handle when save completed
		//Refresh the table
		$scope.modelEdited = function (oldValue, newValue) {
			console.log("model updated");
			console.log('oldValue');
			console.log(oldValue);
			console.log('newValue');
			console.log(newValue);

			//reload table
			//reloadTable();
		};

		$scope.removeModel = function(model) {
			if (confirm('Are you sure to delete ' + model.name + '?')) {
				modelService.remove({id: model.id}, function() {
					console.log('model: ' + model.name + 'was succefully removed');
					reloadTable();
				});
			}
		};

		function reloadTable() {
			console.log('reloading table');
			$scope.tableParams.reload();
		}
	}];

	return {
		restrict: 'E',
		transclude: true,
		replace: false,
		scope: {
			templatePath: '@',
			modelService: '@',
			total: "="
		},
		controller: controller,
		templateUrl: function(element, attrs) {
			return attrs.templatePath;
		},
		link: function(scope, element, attrs) {
		}
	};
});