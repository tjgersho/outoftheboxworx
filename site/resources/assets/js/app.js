angular.module('controllers', []);

angular.module('services', []);

angular.module('directives', []);

angular.module('paradigmMotion', [
	'ui.bootstrap',
	'services',
	'ngTable',
	'ngFileUpload',
	'infinite-scroll',
	'hierarchical-selector',
	'ui.router',
	'ngResource',
	'ngStorage',
	'appRoutes',
	'enterStroke',
	'directives',
	'controllers',
       'ngAnimate'
]);

angular.module('paradigmMotion')
	.filter('dateToISO', function() {
	  return function(badTime) {
	    var goodTime = badTime.replace(/(.+) (.+)/, "$1T$2Z");
	    return goodTime;
	  };
	})
	.filter('rtrimDecimalZeros', function() {
	  return function(n) {
	  	if (n.indexOf(".")) {
	 		var m = n.replace(/0+$/, "").replace(/\.$/,"")
		}
	    return m;
	  };
	})
	.filter('capitalize', function() {
	    return function(input) {
	      return (!!input) ? input.charAt(0).toUpperCase() + input.substr(1).toLowerCase() : '';
	    }
	});
	;