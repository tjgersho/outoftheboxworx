<?php 
function sendWelcomeEmails($people){
	$mandrill = new \Mandrill(\Config::get('services.mandrill.secret'));
	$to = array();
	$merge_vars = array();
	foreach($people as $person){
		$to[] = array(	
						'email' => $person['email'],
			            'name' => $person['name'],
			            'type' => 'to'
			         );
		$merge_vars[] = array(	
								'rcpt' => $person['email'],
					            'vars' => array(
	                                                array('name' => 'EMAIL', 'content' => $person['email']),
	                                                array('name' => 'TEMPPASS', 'content' => $person['password']),
	                                                array('name' => 'GROUP', 'content' => $person['group']),
	                                                array('name' => 'COMPANY', 'content' => $person['company'])
                                                )
					         );
	}
    $result = $mandrill->messages->sendTemplate('Welcome Template', [], array(
                                                                                'to' => $to,
                                                                                'merge' => true,
                                                                                'merge_language' => 'mailchimp',
                                                                                'merge_vars' => $merge_vars,
                                                                            )
                                                );
    //var_dump($result);
}

function sendWelcomeEmailIndividual($person){
	$mandrill = new \Mandrill(\Config::get('services.mandrill.secret'));
	$to = array();
	$merge_vars = array();
	$to[] = array(	
					'email' => $person['email'],
		            'name' => $person['name'],
		            'type' => 'to'
		         );
	$merge_vars[] = array(	
							'rcpt' => $person['email'],
				            'vars' => array(
                                                array('name' => 'EMAIL', 'content' => $person['email']),
                                                array('name' => 'TEMPPASS', 'content' => $person['password']),
                                                array('name' => 'GROUP', 'content' => $person['group']),
                                                array('name' => 'COMPANY', 'content' => $person['company'])
                                            )
				         );
    $result = $mandrill->messages->sendTemplate('Welcome Template Individual Users', [], array(
                                                                                'to' => $to,
                                                                                'merge' => true,
                                                                                'merge_language' => 'mailchimp',
                                                                                'merge_vars' => $merge_vars,
                                                                            )
                                                );
    //var_dump($result);
}
 ?>