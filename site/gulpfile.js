var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */


elixir(function (mix) {
  mix
    .less(
      'app.less', 'resources/assets/css/less.css' 
    )
    .sass(
     'app.scss', 'resources/assets/css/sass.css' 
     )
    .styles([
      '*.css'
       ])
    .scripts([
      'libs/**/*.js',
      'jquery-app.js',
      'app.js',
      'appRoutes.js',
      'controllers/**/*.js',
      'services/**/*.js',
      'directives/**/*.js'
    ])
    .version([
      'css/all.css',
      'js/all.js'
    ])
    .copy(
    'public/js/all.js.map', 'public/build/js/all.js.map'
  )
    .copy(
    'public/css/all.css.map', 'public/build/css/all.css.map'
  )
    .copy(
    'resources/assets/images/', 'public/images/'
  )
    .copy(
    'resources/assets/fonts', 'public/fonts/'
  );
});

